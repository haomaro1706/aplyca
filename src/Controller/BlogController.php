<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Entity\Entrada;
use App\Form\BlogType;
use App\Form\EntradaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/crear-blog", name="app_crearBlog")
     */
    public function index(Request $request)
    {
        /*$blog = new Blog();
        $form = $this->createForm(BlogType::class,$blog);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $user = $this->getUser();
            $blog->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();
            $this->addFlash('exito','Se ha creado correctamente');
            return $this->redirectToRoute('app_home');
        }*/
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $entradas = $em->getRepository(Entrada::class)->findAll(); 
        return $this->render('blog/index.html.twig', [
            'entradas' => $entradas,
            'user' => $user
        ]);
    }

    /**
    * @Route("/crear-entrada", name="app_crearEntrada")
    */
    public function entrada(Request $request)
    {
        $user = $this->getUser();
        //dump($user);
        if($user){
            $entrada = new Entrada();
            $form = $this->createForm(EntradaType::class,$entrada);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $blogEm = $this->getDoctrine()->getManager();
                $blogResult = $blogEm->getRepository(Blog::class)->findOneBy(['user'=>4]);
                $imagen = $form['imagen']->getData();
                if($imagen){
                    $path = $imagen->getPathname();
                    $contenidoBinario = file_get_contents($path);
                    $imagenComoBase64 = base64_encode($contenidoBinario);
                    $base64 = 'data:image/'.$imagen->guessExtension().';base64,'.($imagenComoBase64);
                    $entrada->setImagen($base64);
                } 
                //echo ($blogResult->getId());
                //die();             
                $entrada->setBlo(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entrada);
                $em->flush();
                $this->addFlash('exito','agregado correctamente');
                return $this->redirectToRoute('app_home');
            }
            return $this->render('blog/entrada.html.twig', [
                'form' => $form->createView()
            ]);
        }else{
            $this->addFlash('error','Debe iniciar sesion');
            return $this->redirectToRoute('app_home');
        }

    }

 /**
    * @Route("/editar-entrada", name="app_editarEntrada")
    */
    public function editar(Request $request)
    {
        $user = $this->getUser();
        $id = $request->query->get('id');
        
        if($user){
            $em = $this->getDoctrine()->getManager();
            $entrada = $em->getRepository(Entrada::class)->find($id);
            dump($entrada);
            die();
            $form = $this->createForm(EntradaType::class,$entrada);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $blogEm = $this->getDoctrine()->getManager();
                $blogResult = $blogEm->getRepository(Blog::class)->findOneBy(['user'=>$user]);
                
                $entrada->setBlo(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entrada);
                $em->flush();
                $this->addFlash('exito','agregado correctamente');
                return $this->redirectToRoute('app_home');
            }
            return $this->render('blog/entrada.html.twig', [
                'form' => $form->createView()
            ]);
        }else{
            $this->addFlash('error','Debe iniciar sesion');
            return $this->redirectToRoute('app_home');
        }

    }

}
