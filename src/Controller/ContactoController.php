<?php

namespace App\Controller;

use App\Entity\Contacto;
use App\Form\ContactoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactoController extends AbstractController
{
    /**
     * @Route("/contacto", name="app_contacto")
     */
    public function index(Request $request)
    {
        $contacto = new Contacto();
        $form = $this->createForm(ContactoType::class,$contacto);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            $this->addFlash('exito','Envio realizado correctamente');
            return $this->redirectToRoute('app_contacto');
        }
        return $this->render('contacto/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
