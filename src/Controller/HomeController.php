<?php

namespace App\Controller;

use App\Entity\Blog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        //if($user){
            $em = $this->getDoctrine()->getManager();
            $blogs = $em->getRepository(Blog::class)->findAll();
            //dump($blogs);
            //die();
            return $this->render('home/index.html.twig', [
                'blogs' => $blogs,
                'user' => $user
            ]);
        /*}else{
            return $this->redirectToRoute('app_login');
        }*/
    }
}
